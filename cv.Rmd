---
title: "Pierre Wallet's resume"
author: Pierre Wallet
date: "`r Sys.Date()`"
output:
  pagedown::html_resume:
    css:  ['dd_cv.css', 'resume']
    # set it to true for a self-contained HTML page but it'll take longer to render
    self_contained: false
# uncomment this line to produce HTML and PDF in RStudio:
#knit: pagedown::chrome_print
---

Aside
================================================================================


Contact Info {#contact}
--------------------------------------------------------------------------------

- <i class="fa fa-envelope"></i> pierre_wallet@hotmail.fr
- <i class="fa fa-gitlab" aria-hidden="true"></i> [gitlab/pierrewallet](https://gitlab.com/pierrewallet)
- For more information, please contact me via email.

Skills {#skills}
--------------------------------------------------------------------------------

- Experienced in statistical analysis, statistical programming, and software development.

- Highly skilled in R, SAS, git.

- Excellent written and oral communication skills.


Interests {#interests}
--------------------------------------------------------------------------------

- Triathlon, Trail

- Japanese culture



Disclaimer {#disclaimer}
--------------------------------------------------------------------------------

This resume was made with the R package [**pagedown**](https://github.com/rstudio/pagedown).

Last updated on `r Sys.Date()`.



Main
================================================================================

Pierre Wallet {#title}
--------------------------------------------------------------------------------

###  Enthusiast R Developer

Passionate with R ecosystem, I provide consultancy services to help you develop
your R codes, scale them up through packages and deliver business insights to
end-users using Shiny apps.


Professional Experience {data-icon=suitcase}
--------------------------------------------------------------------------------

### Senior Principal Statistical Programmer

*Novartis, Pharma, 51B€ turnover, 110k headcounts*

Paris, France

2023 - 2019

- Lead trial programmer for Companion Diagnostics and phase I oncology studies 
- R innovation lead
- Training courses to leap from SAS to R: 300+ associate trained
- Package development to support SDTM datasets programming
- Creation of 20+ SDTM datasets validation template programs


### Senior Data Consultant

*Sutter Mills, Consulting, 10M€ turnover, 70 headcounts*

Paris, France

2019 - 2018

- Data architecture and data visualization tool audit and recommendation for a European media company
- Pre-sales analysis (segmentation, scoring) for an international automotive leader
- Supervising and mentoring consultants


### Senior Statistical Programmer

*Boehringer Ingelheim, Pharma, 438M€ turnover, 50000 headcounts*

Reims, France

2017 - 2012

- Lead trial programmer for phases II/III diabetes clinical trial
- Responsible for handling the validation process with external partner
- Responsible for laboratory analyses package within respiratory project, drug OFEV approved by FDA


### Statistics and Marketing Analyst

*Bforbank, Bank, 35M€ net banking income, 150 headcounts*

Paris, France

2012 - 2010

- Life-insurance appetency scoring using website consumption and advisers' contacts
- Segmentation on customers' passbooks
- Customers targeting, campaign reports for upselling (passbooks) and cross-selling (life-insurance)


### Statistics and Marketing Analyst

*Lincoln for EDF, Consulting, 24M€ turnover, 250 headcounts*

Paris, France

2010 - 2008

- Prediction of heating energy type for individual customers: reliability rate 77%
- Professional customers' bankruptcy prevision: model 2.2 times more powerful than chance
- Customers' satisfaction barometer


Education {data-icon=graduation-cap data-concise=true}
--------------------------------------------------------------------------------

### Neoma Business School

International MBA - Business Administration and Management

Paris, France

2018

### University Paris-Sud

M.S. in Statistics

Orsay, France

2007